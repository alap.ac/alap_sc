import Container from '../components/container'
import MoreStories from '../components/more-stories'
import HeroPost from '../components/hero-post'
import HeroPost0 from '../components/hero-post0'
import HeroPost1 from '../components/hero-post1'
import HeroPost2 from '../components/hero-post2'
import HeroPost3 from '../components/hero-post3'
import Intro from '../components/intro'
import Layout from '../components/layout'
import { getAllPosts } from '../lib/api'
import Head from 'next/head'
import Link from 'next/link'
import { CMS_NAME } from '../lib/constants'

export default function Index({ allPosts }) {
  const heroPost0 = allPosts[0]
  const heroPost1 = allPosts[1]
  const heroPost2 = allPosts[2]
const heroPost3 = allPosts[3]
  return (
    <>
      <Layout>
        <Head>
          <title>আলাপ - বিজ্ঞান পরিচয়</title>
        </Head>
        <Container>
          <Intro />

<section className="text-gray-700 body-font">
  <div className="container px-1 mx-auto">
    <div className="flex flex-wrap w-full mb-5 flex-col items-center text-center">
      <h1 className="sm:text-3xl text-2xl font-medium mt-6 title-font text-pink-500">সাম্প্রতিক পোস্ট</h1>
      <p className="lg:w-1/2 w-full leading-relaxed text-base">ক্লাবের সকল খবরাখবর, বিভিন্ন লেখাপত্র</p>
    </div>
    <div className="flex flex-wrap -m-4">
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-pink-500 p-3 rounded-lg">
                {heroPost0 && (
            <HeroPost0
              title={heroPost0.title}
              coverImage={heroPost0.coverImage}
              date={heroPost0.date}
              author={heroPost0.author}
              slug={heroPost0.slug}
              excerpt={heroPost0.excerpt}
            />
)}



        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-pink-500 p-3 rounded-lg">
                    {heroPost1 && (
            <HeroPost1
              title={heroPost1.title}
              coverImage={heroPost1.coverImage}
              date={heroPost1.date}
              author={heroPost1.author}
              slug={heroPost1.slug}
              excerpt={heroPost1.excerpt}
            />
)}

        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-pink-500 p-3 rounded-lg">
          {heroPost2 && (
            <HeroPost2
              title={heroPost2.title}
              coverImage={heroPost2.coverImage}
              date={heroPost2.date}
              author={heroPost2.author}
              slug={heroPost2.slug}
              excerpt={heroPost2.excerpt}
            />
)}

        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-pink-500 p-3 rounded-lg">
          
          {heroPost3 && (
            <HeroPost3
              title={heroPost3.title}
              coverImage={heroPost3.coverImage}
              date={heroPost3.date}
              author={heroPost3.author}
              slug={heroPost3.slug}
              excerpt={heroPost3.excerpt}
            />
)}

        </div>
      </div>
    </div>
<Link  href="/posts">
              <a className="hover:underline">
       <button className="flex mx-auto mt-5 text-white bg-pink-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
  পুরানো পোস্টসমূহ পড়ুন
</button>
          </a>
            </Link>
  </div>
</section>


<section className="text-gray-700 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-20">
  
      <h1 className="sm:text-3xl text-2xl font-medium title-font text-pink-500">পরিচয়</h1>
      <p className="lg:w-2/3 mx-auto leading-relaxed text-base">ব্রেকথ্রু সায়েন্স সোসাইটি'র সাথে যুক্ত বিজ্ঞান ক্লাব</p>
    </div>
    <div className="flex flex-wrap">
      <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200">
        <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">ইতিহাস</h2>
        <p className="leading-relaxed text-base mb-4"></p>
        
      </div>
      <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200">
        <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">লক্ষ ও উদ্দেশ্য</h2>
        <p className="leading-relaxed text-base mb-4"></p>
        
      </div>
      <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200">
        <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">কর্মসূচী</h2>
        <p className="leading-relaxed text-base mb-4"></p>
        
      </div>
      <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200">
        <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">নিয়মাবলী</h2>
        <p className="leading-relaxed text-base mb-4"></p>
        
      </div>
    </div>
    <button className="flex mx-auto mt-5 text-white bg-pink-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
  বিস্তারিত পড়ুন
</button>
  </div>
</section>


<section className="text-gray-700 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-20">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-pink-500">ক্লাবের সদস্যবৃন্দ</h1>
      <p className="lg:w-2/3 mx-auto leading-relaxed text-base"></p>
    </div>
    <div className="flex flex-wrap -m-2">
      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/84x84"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">শশাঙ্ক শেখর খামারু</h2>
            <p className="text-gray-500">প্রাথমিক শিক্ষক</p>
          </div>
        </div>
      </div>
      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/88x88"/>

          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">সব্যসাচী হালদার</h2>
            <p className="text-gray-500">প্রাথমিক শিক্ষক</p>
          </div>
        </div>
      </div>
     </div>
<button className="flex mx-auto mt-5 text-white bg-pink-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded-full text-lg">
  সকল সদস্যবৃন্দ সম্পর্কে জানুন
</button>
  </div>
</section>
          
        </Container>
      </Layout>
    </>
  )
}

export async function getStaticProps() {
  const allPosts = getAllPosts([
    'title',
    'date',
    'slug',
    'author',
    'coverImage',
    'excerpt',
  ])

  return {
    props: { allPosts },
  }
}
