import Container from './container'
import { EXAMPLE_PATH } from '../lib/constants'

export default function Footer() {
  return (
    <footer className="bg-accent-1 border-t border-accent-2">
      <Container>
        <section className="flex-col flex items-center">
      <h1 className="text-xl font-bold tracking-tighter leading-tight">
        যোগাযোগ
      </h1>
      <h4 className="text-center text-lg -mt-1mb-3">
        Email: alap.ac@criptext.com
      </h4>
    </section>
      </Container>
    </footer>
  )
}
