import { CMS_NAME } from '../lib/constants'

export default function Intro() {
  return (
    <section className="flex-col flex bg-pink-500 items-center mt-12 mb-5">
      <h1 className="text-5xl font-bold text-gray-100 tracking-wide leading-tight">
        আলাপ
      </h1>
      <h4 className="text-center text-gray-300 text-xl -mt-1">
        বিজ্ঞান পরিচয়
      </h4>
    </section>
  )
}
