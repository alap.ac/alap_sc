import Avatar from '../components/avatar'
import DateFormater from '../components/date-formater'
import CoverImage from '../components/cover-image'
import Link from 'next/link'

export default function HeroPost1({
  title,
  coverImage,
  date,
  excerpt,
  author,
  slug,
}) {
  return (
    <section>
      
        <div>
          <p className="text-lg text-justify leading-relaxed mb-4">{excerpt}</p>
          
      </div>
<Link as={`/posts/${slug}`} href="/posts/[slug]">
              <a className="hover:underline"><button className="bg-pink-500 hover:bg-blue-700 text-white font-bold px-4 rounded-full">
  বিস্তারিত পড়ুন
</button>
            </a>
            </Link>
    </section>
  )
}
