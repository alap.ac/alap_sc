import Avatar from '../components/avatar'
import DateFormater from '../components/date-formater'
import CoverImage from '../components/cover-image'
import Link from 'next/link'

export default function HeroPost({
  title,
  coverImage,
  date,
  excerpt,
  author,
  slug,
}) {
  return (
    <section>
         <div className="py-8 flex flex-wrap md:flex-no-wrap">
        <div className="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
          <span className="tracking-widest font-medium title-font text-gray-900">CATEGORY</span>
          <span className="mt-1 text-gray-500 text-sm"><DateFormater dateString={date} /></span>
        </div>
        <div className="md:flex-grow">
          <h2 className="text-2xl font-medium text-gray-900 title-font mb-2"><Link as={`/posts/${slug}`} href="/posts/[slug]">
          <a className="hover:underline">{title}</a>
        </Link></h2>
<p className="text-lg leading-relaxed mb-4">{excerpt}</p>
                  <Link as={`/posts/${slug}`} href="/posts/[slug]">
          <a className="hover:underline">বিস্তারিত পড়ুন</a>
        </Link>
        </div>
      </div>
    </section>
  )
}
